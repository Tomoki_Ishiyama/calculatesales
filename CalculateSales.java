package jp.alhinc.ishiyama_tomoki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class CalculateSales {

	public static void main(String[] args) {

		/****************************************/
		/*　　　　　　ファイル読込　　　　　　　*/
		/****************************************/

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			System.exit(1);
		}

		final int CHECK_PROFIT_NAME =8;
		final String RESULT_FILE_PASS = args[0]+"\\"+"branch.out";
		BufferedReader br = null;

		ArrayList<String> values = new ArrayList<String>();
		ArrayList<String> keys = new ArrayList<String>();
		try {

			File branchFile = new File(args[0] + "\\branch.lst");
			if(!branchFile.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			//	File file = new File("C:\\Users\\ishiyama.tomoki\\Documents\\CalculateSales\\branch.lst");

			FileReader fr = new FileReader(branchFile);
			br = new BufferedReader(fr);

			//ファイルのエンコード内容を確認　必要に応じてコメントアウト
			//System.out.println("Your character code is ->" + System.getProperty("file.encoding"));
			/*
			 * これは単に文字列を出力するコード　改良としてMapに格納する
				String inputLine;
				while ((inputLine = br.readLine()) != null){
					System.out.println(inputLine);
				}
			 */

			// 以下、branch 入力されたブランチをコードと支店に分け、Mapでキーとして扱えるようにする
			// String[] keys = new String[]; これを使うと支店数が変更したときに対応できない、自由に拡張できるListを使う
			String inputLine;

			//int count = 0;
			while( (inputLine = br.readLine()) != null) {	//while には継続条件をかく
				String[] inputLineDivide = inputLine.split(",");

				if((!inputLine.matches("^[0-9]{3},.+$")) || inputLineDivide.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					System.out.println(inputLine);
					br.close();
					return;
				}

				for (int i = 0; i < inputLineDivide.length; i++) {
					//System.out.println(inputLineDivide[i]);
					if(i%2 == 0) { //支店コードの場合
						keys.add(inputLineDivide[i]);
					}else if (i%2 == 1) {
						values.add(inputLineDivide[i]);
					}
				}
				//count++;
			}
		}catch(Exception e) {
			System.out.println(e);
			System.out.println("予期せぬエラーが発生しました");

		}finally {
			if ( br != null) {
				try {
					br.close();

				}catch(	IOException e ) {
					System.out.println("close出来ませんでした");
				}
			}
		}


		//	System.out.println("these keys are brandh code list");
		//		System.out.println(keys);

		//	System.out.println(" these values are earning list about key");
		//		System.out.println(values);


		//Map<キーの型名, 値の型名> オブジェクト名 = new HashMap<>();
		//size 関数で格納されている要素数がわかる
		Map<String,String> branchMap = new HashMap<String,String>();

		for (int i=0; i<values.size(); i++) {
			branchMap.put(keys.get(i), values.get(i));
		}


		/****************************************/
		/*　　　　　　　　　集計　　　　　　　　*/
		/****************************************/

		//集計用ファイルの検索
		//ファイルの拡張子がRCD　→　ファイルの桁数が８桁
		//
		File profitFile = new File(args[0]);
		File[] profitFileList = profitFile.listFiles();
		String fileName;
		String[] onlyFileName;
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> avarableNames = new ArrayList<String>();

		if (profitFileList != null) {
			for (int i = 0; i < profitFileList.length; i++) {
				fileName = profitFileList[i].getName(); //getName()を使うことで絶対パス表示→ファイル名のみの表示に切り替え
				//System.out.println(fileName);
				if(fileName.contains("rcd")) {
					onlyFileName = fileName.split("\\."); //.だけだと正規表現「任意の一文字」になり、エスケープ演算子で除外　Pattern.quote(".")　fileName.matches("^[0-9]{8}.rcd$")
					if(onlyFileName[0].length() == CHECK_PROFIT_NAME) {//ファイル名が8桁か。配列.lengthは配列の要素の数を返す。文字列.length()は文字列の文字数を返す。
						//正常なファイルの名前をリストにいれてる
						avarableNames.add(onlyFileName[0]+".rcd");
						names.add(onlyFileName[0]);
					}
				}
			}
		}


		//売上ファイル名の連番判定をするぶぶん　if you want to make a method :
		//boolean
		String[] nm = new String[names.size()];
		for (int i=1; i<nm.length;i++ ) {
			int prename = Integer.parseInt(names.get(i-1));
			int currname = Integer.parseInt(names.get(i));
			//System.out.println("prename : "+ prename);
			//System.out.println("currname :"+ currname);

			if(prename + 1 != currname ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		values.clear();
		keys.clear();
		//TreeMapにすることでキーを昇順に並び替える
		Map<String,Long> codeAndProfitMap = new TreeMap<String,Long>();


		//上で集めたファイル名のリストから順次ファイルリーダのぱすをつなぐ
		//あえて拡張For文で書いてみる
		int index = 0;
		for(String name : avarableNames){

			File file = new File(args[0] +"\\" +name);
			try {
				FileReader filereader = new FileReader(file);
				BufferedReader buffer = new BufferedReader(filereader);
				String line ;

				int i = 0; //行数
				//ファイルの行分くりかえす　iは行数
				while((line = buffer.readLine()) != null) {

					//System.out.println(file.getName());
					//System.out.println("value of i :" +i);
					if(i >= 2) {//iは行数　行数が2でないときエラー 3行超えた時点で終了

						System.out.println("<" + file.getName() +">のフォーマットが不正です");
						//System.out.println(file.getName());
						buffer.close();
						return;
					}

					if(i %2 == 0) {//キーを読み込む load a key
						//エラー処理

						if (!branchMap.containsKey(line)) {
							System.out.println("<" +file.getName()+">の支店コードが不正です");
							//	System.out.println(file.getName());
							buffer.close();
							return;
						}

						keys.add(line);
					}else {//値を読み込む load a value

						if(!line.matches("^[0-9]+$")){//数値でない場合 +を入れて少なくとも1桁
							System.out.println("予期せぬエラーが発生しました");
							buffer.close();
							return;
						}

						if(Long.parseLong(line) >= 10000000000L) { //書き間違い　line.parseLong >= 1000000000
							System.out.println("合計金額が10桁を超えてます");
							System.out.println(file.getName());
							buffer.close();
							return;
						}

						values.add(line);
					}
					i++;
				}

				//System.out.println("value of i :" +i);
				if(i != 2) {//iは行数　行数が2でないときエラー

					System.out.println("<" + file.getName() +">のフォーマットが不正です");
					//System.out.println(file.getName());
					buffer.close();
					return;
				}

				//キーがマップの中にあるかどうか
				if(codeAndProfitMap.containsKey(keys.get(index))){
					//あると代入されちゃうので、一度値を取り出し、新たな値を足す。
					long temp = codeAndProfitMap.get(keys.get(index)) + Long.parseLong(values.get(index),10);
					//System.out.println("Here is one of the total earn about branch :"+temp);

					//codeAndProfitMap.remove(keys.get(index));//Keyに上書きされるからわざわざRemoveし無くてもいい気がする
					codeAndProfitMap.put(keys.get(index),temp);
					//System.out.println("key : "+keys.get(index));
					//System.out.println("value :"+values.get(index));
				}else {
					codeAndProfitMap.put(keys.get(index), Long.valueOf(values.get(index)));

					//System.out.println("key : "+keys.get(index));
					//System.out.println("value :"+values.get(index));

				}

				if(codeAndProfitMap.get(keys.get(index)) >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					System.out.println(file.getName());
					buffer.close();
					return;
				}

				buffer.close();
			} catch(IOException e) {
				System.out.println(e);
			}finally {

			}
			index++;

		}


		System.out.println("/****************************************/");
		System.out.println("/*　　　　　　読込内容確認　　　　　　　*/");
		System.out.println("/****************************************/");
		System.out.println("Here is Map of the branch code and a branch name :");
		System.out.println(branchMap);
		System.out.println("Total profit of each of the branch code:");
		System.out.println(codeAndProfitMap);


		/****************************************/
		/*　　　　　集計結果出力内容　　　　　　*/
		/****************************************/

		BufferedWriter bw = null;
		try {

			File outputFile = new File(RESULT_FILE_PASS);
			FileWriter f = new FileWriter(outputFile);
			bw = new BufferedWriter(f);

			//System.out.println(RESULT_FILE_PASS);
			System.out.println("/****************************************/");
			System.out.println("/*　　　　　　結果出力内容　　　　　　　*/");
			System.out.println("/****************************************/");

			//以下　キーのぶんだけ繰り返し、ファイルへ書き込み

			/*			try {
				codeAndProfitMap.forEach((key,val) ->{

					System.out.printf("%s,%s,%d\n",key,branchMap.get(key),val);
					bw.write(key+","+branchMap.get(key)+","+Long.toString(val));
					bw.newLine();

				});
			}catch(Exception e){
				System.out.println(e);
				return;
			}finally {
				if(bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
						return;
					}
				}
			}
			 */
			for(String key:codeAndProfitMap.keySet()) {
				System.out.printf("%s,%s,%d\n",key,branchMap.get(key),codeAndProfitMap.get(key));
				bw.write(key+","+branchMap.get(key)+","+Long.toString(codeAndProfitMap.get(key)));
				bw.newLine();

			}


		} catch (IOException e) {

			System.out.println(e);
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}

}